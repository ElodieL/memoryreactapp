import PropTypes from 'prop-types'
import React, { useState } from 'react'
import './HighScoreInput.scss';
import { saveHOFEntry } from '../HallOfFame/HallOfFame';


const HighScoreInput = ({guesses, onStored}) => {
    const [winner, setWinner] = useState('')

    
    const handleWinnerOnChange = event => {
       setWinner(event.target.value.toUpperCase())
    }

    const persistWinner = event => {
        event.preventDefault()
        const newEntry = {guesses: guesses, player: winner}
        saveHOFEntry(newEntry, onStored)
    }


    return (
        <form className="highScoreInput" onSubmit={persistWinner}>
            <p>
                <label>
                    Bravo ! Entre ton prénom :
                    <input
                        type="text" 
                        autoComplete="given-name" 
                        value={winner}
                        onChange={handleWinnerOnChange}
                    />
                </label>
                <button type="submit">J’ai gagné !</button>
            </p>
        </form>
    )
}

HighScoreInput.propTypes = {
  guesses: PropTypes.number.isRequired,
  onStored: PropTypes.func.isRequired,
}

export default HighScoreInput;

/*class HighScoreInput extends Component {
    state = { winner: ''}

    handleWinnerOnChange = event => {
        this.setState({winner: event.target.value.toUpperCase()})
    }

    persistWinner = event => {
        event.preventDefault()
        const newEntry = {guesses: this.props.guesses, player: this.state.winner}
        saveHOFEntry(newEntry, this.props.onStored)
    }

    render() {
        return (
        <form className="highScoreInput" onSubmit={this.persistWinner}>
            <p>
            <label>
                Bravo ! Entre ton prénom :
                <input
                    type="text" 
                    autoComplete="given-name" 
                    value={this.state.winner}
                    onChange={this.handleWinnerOnChange}
                />
            </label>
            <button type="submit">J’ai gagné !</button>
            </p>
        </form>
        )
    }
}

HighScoreInput.propTypes = {
  guesses: PropTypes.number.isRequired,
  onStored: PropTypes.func.isRequired,
}

export default HighScoreInput;*/
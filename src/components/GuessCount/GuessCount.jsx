import React from 'react';
import PropTypes from 'prop-types';
import './GuessCount.scss';

const GuessCount = ({guesses}) => <div className="guesses">{guesses}</div>

GuessCount.propTypes = {
    guesses: PropTypes.number.isRequired,
}

GuessCount.defaulProps = {
    guesses: 0
}
 
export default GuessCount;
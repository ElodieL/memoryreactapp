import React, { Component, useState } from 'react';
import shuffle from 'lodash.shuffle'
import './App.scss';
import GuessCount from '../components/GuessCount/GuessCount';
import Card from '../components/Card/Card';
import HallOfFame from '../components/HallOfFame/HallOfFame';
import HighScoreInput from '../components/HighScoreInput/HighScoreInput';

const SIDE = 6
export const SYMBOLS = '😀🎉💖🎩🐶🐱🦄🐬🌍🌛🌞💫🍎🍌🍓🍐🍟🍿'
const VISUAL_PAUSE_MSECS = 750
/*const generateCards = () => {
      const result = []
      const size = SIDE * SIDE
      const candidates = shuffle(SYMBOLS)
      while (result.length < size) {
        const card = candidates.pop()
        result.push(card, card)
      }
      return shuffle(result)
    }

const App = () =>  {
  const [currentPair, setCurrentPair] = useState([]);
  const [guesses, setGuesses] = useState(0);
  const [hallOfFame, setHallOfFame] = useState(null);
  const [matchedCardIndices, setMatchedCardIndices] = useState([]);
  const [cards] = useState(generateCards());
  const won = matchedCardIndices.length === 4 //cards.length;
  
  const displayHallOfFame = hallOfFame => {
    setHallOfFame(hallOfFame);
  }

  const handleCardClick = index => {
    if (currentPair.length === 2) {
      return
    }
    if (currentPair.length === 0) {
      setCurrentPair([index]);
      return
    }
    handleNewPairClosedBy(index)
  }

  const getFeedbackForCard = index => {
    const indexMatched = matchedCardIndices.includes(index);
    if (currentPair.length < 2) {
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
    }
    if (currentPair.includes(index)) {
      return indexMatched ? 'justMatched' : 'justMismatched'
    }
    return indexMatched ? 'visible' : 'hidden'
  }
    
  const handleNewPairClosedBy = index => {
    const newPair = [currentPair[0], index]
    const newGuesses = guesses + 1
    const matched = cards[newPair[0]] === cards[newPair[1]]
    setCurrentPair(newPair)
    setGuesses(newGuesses)
    if (matched) {
      setMatchedCardIndices([...matchedCardIndices, ...newPair])
    }
    setTimeout(() => setCurrentPair([]), VISUAL_PAUSE_MSECS)
  }

  return ( 
    <div className="memory">
      <GuessCount guesses={guesses}/>
      {cards.map((card, index) => (
        <Card 
          card={card} 
          key={index} 
          index={index}
          feedback={getFeedbackForCard(index)} 
          onClick={handleCardClick}/>
      ))}
      {won &&
        (hallOfFame ? (
          <HallOfFame entries={hallOfFame} />
        ) : (
          <HighScoreInput 
            guesses={guesses} 
            onStored={displayHallOfFame} />
        ))}
    </div>
  );
}
export default App;*/



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cards: this.generateCards(),
      currentPair: [],
      guesses: 0,
      hallOfFame: null,
      matchedCardIndices: [],
    }
  } 

  displayHallOfFame = hallOfFame => {
    this.setState({hallOfFame})
  }

  generateCards() {
    const result = []
    const size = SIDE * SIDE
    const candidates = shuffle(SYMBOLS)
    while (result.length < size) {
      const card = candidates.pop()
      result.push(card, card)
    }
    return shuffle(result)
  }

  getFeedbackForCard(index) {
    const { currentPair, matchedCardIndices } = this.state
    const indexMatched = matchedCardIndices.includes(index)
  
    if (currentPair.length < 2) {
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
    }
  
    if (currentPair.includes(index)) {
      return indexMatched ? 'justMatched' : 'justMismatched'
    }
  
    return indexMatched ? 'visible' : 'hidden'
  }

  handleCardClick = index => {
    const { currentPair } = this.state
  
    if (currentPair.length === 2) {
      return
    }
  
    if (currentPair.length === 0) {
      this.setState({ currentPair: [index] })
      return
    }
  
    this.handleNewPairClosedBy(index)
  }
  
  handleNewPairClosedBy(index) {
    const { cards, currentPair, guesses, matchedCardIndices } = this.state
    const newPair = [currentPair[0], index]
    const newGuesses = guesses + 1
    const matched = cards[newPair[0]] === cards[newPair[1]]
    this.setState({ currentPair: newPair, guesses: newGuesses })
    if (matched) {
      this.setState({ matchedCardIndices: [...matchedCardIndices, ...newPair] })
    }
    setTimeout(() => this.setState({ currentPair: [] }), VISUAL_PAUSE_MSECS)
  }
    
  render() { 
    const { cards, guesses, hallOfFame, matchedCardIndices } = this.state;
    const won = matchedCardIndices.length === 4 //cards.length;
    return ( 
      <div className="memory">
        <GuessCount guesses={guesses}/>
        {cards.map((card, index) => (
          <Card 
            card={card} 
            key={index} 
            index={index}
            feedback={this.getFeedbackForCard(index)} 
            onClick={this.handleCardClick}/>
        ))}
        {won &&
          (hallOfFame ? (
            <HallOfFame entries={hallOfFame} />
          ) : (
            <HighScoreInput 
              guesses={guesses} 
              onStored={this.displayHallOfFame} />
          ))}
        </div>
    )
  }
}
 
export default App;